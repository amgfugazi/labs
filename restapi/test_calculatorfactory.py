"""Тестування фабрики калькулятора."""


from labs import calculatorfactory


def test_select_operation():
    """Тестування отримання функції."""
    assert calculatorfactory.select_operation(1).__name__ == 'sub'
    assert calculatorfactory.select_operation(2).__name__ == 'sum'
