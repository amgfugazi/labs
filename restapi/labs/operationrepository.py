"""Репозиторій операцій."""


from labs import calculator
from sqlalchemy import *
from sqlalchemy.orm import Session
from labs.entities import Operation
from labs.repositorybase import RepositoryBase


class OperationRepository(RepositoryBase):
    """Репозиторій операцій."""

    def __init__(self):
        """Репозиторій операцій."""
        super().__init__()
        with Session(self.engine) as session:
            for operation in [calculator.sub, calculator.sum, calculator.div, calculator.mult]:
                select_query = select([func.count()]).select_from(Operation).where(Operation.operation_name == operation.__doc__)
                res = session.execute(select_query).scalar()
                if res == 0:
                    stmt = (
                        insert(Operation).
                        values(operation_name=operation.__doc__)
                    )
                    session.execute(stmt)
            session.commit()

    def read_operations(self):
        """Читання всіх операцій."""
        with Session(self.engine) as session:
            return session.query(Operation).all()
