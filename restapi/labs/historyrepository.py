"""Провайдер даних."""


from sqlalchemy import *
from sqlalchemy.orm import Session
from labs.entities import Historу
from labs.operationrepository import Operation
from labs.repositorybase import RepositoryBase
from sqlalchemy import orm


class HistoryRepository(RepositoryBase):
    """Провайдер даних."""

    def __init__(self):
        """Конструктор."""
        super().__init__()

    def read_history(self):
        """Читання історії по id."""
        with Session(self.engine) as session:
            result = session.query(Historу).options(orm.joinedload("operation")).all()
            return result

    def add_to_history(self, messages):
        """Запис в історію."""
        with Session(self.engine) as session:
            res = session.query(Operation).where(Operation.operation_name == messages[0]).one()
            stmt = (
                insert(Historу).values(
                    operation_id=res.id,
                    input_a=messages[1],
                    input_b=messages[2],
                    result=messages[3]
                )
            )
            session.execute(stmt)
            session.commit()
