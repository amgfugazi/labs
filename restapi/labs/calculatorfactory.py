"""'Фабрика' калькулятора."""


from labs import calculator


def select_operation(selected_operation):
    """
    Функція для вибору методя з кальтулатора.

    :returns: метод, яким будуть поводитись розрахунки
    """
    match selected_operation:
        case 1:
            return calculator.sub
        case 2:
            return calculator.sum
        case 3:
            return calculator.div
        case 4:
            return calculator.mult
        case _:
            raise Exception("Invalid operation")
