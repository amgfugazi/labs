"""Конфігурація."""


import configparser
import os


class Configuration:
    """Конфігурація."""

    FILE_NAME = "config.ini"

    def __init__(self):
        """Ініціалізація."""
        dir = os.path.dirname(__file__)
        filename = os.path.join(dir, self.FILE_NAME)
        self.config = configparser.ConfigParser()
        self.config.sections()
        self.config.read(filename)

    def read_connection_string(self):
        """Читання стрічки підключення."""
        return self.config["DB"]["ConnectionString"]
