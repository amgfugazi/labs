"""Головна програма."""

from labs import calculatorfactory
from fastapi import FastAPI
from pydantic import BaseModel
from fastapi import status
from fastapi.responses import JSONResponse
from labs.historyrepository import HistoryRepository
from labs.operationrepository import OperationRepository
from fastapi.middleware.cors import CORSMiddleware

class CalculateItem(BaseModel):
    operation_id: int
    input_a: float
    input_b: float

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

history_repository = HistoryRepository()
operation_repository = OperationRepository()

@app.get("/")
def read_root():
    return "Колькулятор"

@app.get("/operations")
def read_all_operations():
    res = operation_repository.read_operations()
    return res

@app.get("/history")
def read_all_history():
    res = history_repository.read_history()
    return res

@app.post("/calculate")
def read_item(item: CalculateItem):
    function = calculatorfactory.select_operation(item.operation_id)
    result = function(item.input_a, item.input_b)
    data = []
    data.append(function.__doc__)
    data.append(item.input_a)
    data.append(item.input_b)
    data.append(result)
    history_repository.add_to_history(data)
    return {"Result": result}
