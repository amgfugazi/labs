from setuptools import setup


setup(
    name='labs',
    version='0.0.1',
    license='MIT',
    author='Sasha',
    author_email='karnaukhov@live.com',
    description='webui',
    include_package_data=True
)
