"""Web UI."""


import streamlit as st
import pandas as pd
import requests

BASE_URL = "http://calcapi:80/"
OPERATIONS_URL = f"{BASE_URL}operations"
CALCULATE_URL = f"{BASE_URL}calculate"
HISTORY_URL = f"{BASE_URL}history"

st.title("Калькулятор")


def fetch_operations():
    """Читання операцій."""
    try:
        response = requests.get(OPERATIONS_URL).json()
        return response
    except Exception as e:
        raise e


def operation_format_function(item):
    """Форматування стрічки операції."""
    return item["operation_name"]


def fetch_history():
    """Читання історії."""
    try:
        response = requests.get(HISTORY_URL).json()
        return response
    except Exception as e:
        raise e


def calculate(operation_id: int, input_a: float, input_b: float):
    """Виклик веб методу для калькуляції."""
    try:
        result = requests.post(CALCULATE_URL, json={"operation_id": operation_id, "input_a": input_a, "input_b": input_b})
        st.write(result.json()["Result"])
    except Exception as e:
        raise e


res = fetch_operations()
selected_operation = st.selectbox("Оберіть операцію", res, format_func=operation_format_function)
input_a = st.text_input("Значення а")
input_b = st.text_input("Значення b")

if (st.button("Порахувати")):
    calculate(selected_operation["id"], input_a, input_b)

if (st.button("Історія")):
    history = fetch_history()
    hdf = pd.json_normalize(history, max_level=1)
    hdf = hdf.reindex(columns=["id", "operation.operation_name", "input_a", "input_b", "result"])
    hdf.rename(columns={"input_a": "a", "input_b": "b", "result": "Результат", "id": "#", "operation.operation_name": "операція"}, inplace=True)
    st.write("## Історія")
    st.table(hdf)
